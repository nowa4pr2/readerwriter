package at.campus02.nowa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConsoleCsvReader {

	public static void main(String[] args) {

		String line;

		try (BufferedReader br = new BufferedReader(new FileReader(new File("C:\\devdata\\PR2\\personen.csv")))) {

			List<Person> persons = new ArrayList<>();

			while ((line = br.readLine()) != null) {
				String[] splited = line.split(",");

				Person p = new Person(splited[0], splited[1], splited[2]);

				persons.add(p);
			}

			for (Person person : persons) {
				System.out.println(person);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
