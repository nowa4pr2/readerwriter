package at.campus02.nowa;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleFileWriter {
	public static void main(String[] args) {
		
		BufferedReader br = null;
		FileWriter pw = null;
		
		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			pw = new FileWriter("D:\\PR2\\noten.txt");
			
			
			String line;
			while((line = br.readLine()) != null) {
				
				
				if (!line.equals("STOP"))  {
					pw.write(line + "\r\n");
				} else {
					break;
				}
			}
			pw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				pw.close();
			} catch (IOException e) {
				// TODO A uto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
