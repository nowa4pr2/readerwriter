 package at.campus02.nowa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReaderApp {

	public static void main(String[] main) {
		
			BufferedReader br = null;		
			
			try {
				br = new BufferedReader(new InputStreamReader(System.in));
				
				String line;
				while((line = br.readLine()) != null) {
					System.out.println(line);
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}
}
